from django.urls import path

from meal_plans.views import (
    MealPlanListView,
    MealPlanDeleteView,
    MealPlanDetailView,
    MealplanCreateView,
    MealPlanUpdateView,
)


urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plans_list"),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plans_delete",
    ),
    path("new/", MealplanCreateView.as_view(), name="meal_plan_new"),
    path("<int:pk>/", MealPlanDetailView.as_view(), name="meal_plan_detail"),
    path("<int:pk>/edit/", MealPlanUpdateView.as_view(), name="meal_plan_edit"),
]
